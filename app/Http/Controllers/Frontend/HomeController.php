<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    // home
    public function index() {
        return view('frontend.index');
    }
    /**
     * Register
     */
    public function register() {
        return view('frontend.login.register');
    }
    /**
     * Login
     */
    public function login() {
        return view('frontend.login.login');
    }
    /**
     * Detail
     */
    public function detail()
    {
        return view('frontend.category.detail');
    }
}
