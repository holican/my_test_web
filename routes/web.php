<?php

use Illuminate\Support\Facades\Input;

Auth::routes(['verify' => true]);
/*------------------------------------------------------------------------*/
/* Frontend Routes*/
/*------------------------------------------------------------------------*/
Route::get('/', 'Frontend\HomeController@index')->name('home');
Route::get('/register', 'Frontend\HomeController@register')->name('register');
Route::get('/login', 'Frontend\HomeController@login')->name('login');

Route::get('/courses', 'Frontend\HomeController@detail')->name('courses');


