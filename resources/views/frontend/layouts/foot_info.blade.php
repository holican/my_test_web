<div class="mt-5 footer-container footer py-4">
    <div class="container">
        <div class="row d-flex px-5">
            <div class="col-12 col-md-4 mb-2">
                <span class="foot-title">ADDRESS:</span> <br>
                <small>No.9A, Baho Road</small> <br>
                <small> San Chaung Township,</small> <br>
                <small>Yangon.</small>
            </div>
            <div class="col-12 col-md-4 mb-2">
                <span class="foot-title">HOTLINES:</span> <br>
                <small>+95 250717166 | +95 250717168</small> <br><br>
                <span class="foot-title">OPENING HOURS:</span> <br>
                <small>Monday - Sunday / 09.00AM - 19.00PM</small>
            </div>
            <div class="col-12 col-md-4 mb-2">
                <span class="foot-title">EMAILS:</span> <br>
                <small>info@strategyfirst.edu.mm</small> <br><br>
                <span class="foot-title">FOLLOW US ON:</span> <br>
                <i class="fab fa-facebook-square social"></i>&nbsp;
                <i class="fab fa-twitter-square social"></i>&nbsp;
                <i class="fab fa-instagram social"></i>
            </div>
        </div>
    </div>
</div>