@include('frontend.layouts.header')
<!-- <div class="container main"> -->
    @include('frontend.layouts.nav')
    <main>
        <div id="app">
            @yield('content')
        </div>
    </main>
<!-- </div> -->
@include('frontend.layouts.foot_info')
@include('frontend.layouts.footer')