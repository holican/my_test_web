require("./bootstrap");

import Vue from "vue";
import axios from "axios";
import VueAwesomeSwiper from 'vue-awesome-swiper';
import "swiper/css/swiper.css";
Vue.use(VueAwesomeSwiper);
Vue.component(
    "home",
    require("./components/HomeComponent.vue").default
);
Vue.component(
    "detail",
    require("./components/DetailComponent.vue").default
);
Vue.component(
    "navbar",
    require("./components/NavComponent.vue").default
);
Vue.component(
    "login",
    require("./components/login/LoginComponent.vue").default
);
Vue.component(
    "register",
    require("./components/login/RegisterComponent.vue").default
);
Vue.component(
    "banner-slider",
    require("./components/BannerSliderComponent.vue").default
);
Vue.component(
    "inform-slider",
    require("./components/InformSliderComponent.vue").default
);
const app = new Vue({
    el: "#app",
});
const navbar = new Vue({
    el: "#navbar",
});